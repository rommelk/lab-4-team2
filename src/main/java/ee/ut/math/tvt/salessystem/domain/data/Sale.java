package ee.ut.math.tvt.salessystem.domain.data;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.*;

@Entity
@Table(name = "SALE")
public class Sale  implements DisplayableItem {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;

    @Column(name = "TIME")
    private Date saleTime;

    @Column(name = "TOTALSUM")
    private double totalSum;

    @OneToMany(mappedBy = "sale", fetch=FetchType.EAGER)
    private List<SoldItem> shoppingCart = new ArrayList<>();


/*    public Sale(Long id, Date saleTime, double totalSum) {
        this.saleTime = saleTime;
        this.totalSum = totalSum;
        this.id = id;

    }*/

    public Sale() {
    }

    public Sale(Long id, Date saleTime, double totalSum, List<SoldItem> shoppingCart) {
        this.id = id;
        this.saleTime = saleTime;
        this.totalSum = totalSum;
        this.shoppingCart = shoppingCart;
    }

    public Sale(Long id, Date saleTime, List<SoldItem> shoppingCart) {
        this.id = id;
        this.saleTime = saleTime;
        this.shoppingCart = shoppingCart;
        this.totalSum = getTotalSum();
    }

    public List<SoldItem>getShoppingCart() {
        return shoppingCart;
    }

    public void setShoppingCart(List<SoldItem> shoppingCart) {
        this.shoppingCart = shoppingCart;
    }


    public Date getSaleTime() {
        return saleTime;
    }

/*    public double getTotalSum() {
        return totalSum;
    }*/

    public double getTotalSum() {
        double sum = 0;
        for (SoldItem soldItem : shoppingCart) {
            sum = sum + soldItem.getSum();
        }
        return sum;
    }

    public void setSaleTime(Date saleTime) {
        this.saleTime = saleTime;
    }

    public void setTotalSum(Double totalSum) {
        this.totalSum = totalSum;
    }

    @Override
    public Long getId() {
        return id;
    }

/*    public Object clone() {
        PurchaseItem item = new PurchaseItem(getSaleTime(), getTotalSum());
        return item;
    }*/

    public void addSoldItem(SoldItem soldItem) {
        shoppingCart.add(soldItem);
    }

    @Override
    public String toString() {
        return "PurchaseItem{" +
                "id=" + id +
                ", saleTime=" + saleTime +
                ", totalSum=" + totalSum +
                '}';
    }
}
