package ee.ut.math.tvt.salessystem.service;

import ee.ut.math.tvt.salessystem.domain.data.Sale;
import ee.ut.math.tvt.salessystem.domain.data.SoldItem;
import ee.ut.math.tvt.salessystem.domain.data.StockItem;
import ee.ut.math.tvt.salessystem.util.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.Collections;
import java.util.List;

@SuppressWarnings("unchecked")
public class HibernateDataService {

    private Session session = HibernateUtil.currentSession();

    public List<StockItem> getStockitems() {
        List<StockItem> stockItems = Collections.checkedList(session.createQuery("from StockItem").list(), StockItem.class);
        return stockItems;
    }

    public List<Sale> getSaleitems() {
        List<Sale> saleItems = Collections.checkedList(session.createQuery("from Sale").list(), Sale.class);
        return saleItems;
    }

    public List<SoldItem> getSolditems() {
        List<SoldItem> soldItems = Collections.checkedList(session.createQuery("from SoldItem").list(), SoldItem.class);
        return soldItems;
    }

    public void addStockItem(StockItem stockItem) {
        Transaction transaction = session.beginTransaction();
        session.save(stockItem);
        transaction.commit();
    }

    public void addSoldItem(SoldItem soldItem) {
        Transaction transaction = session.beginTransaction();
        session.save(soldItem);
        transaction.commit();
    }

    public void addSale(Sale sale) {
        Transaction transaction = session.beginTransaction();
        session.save(sale);
        transaction.commit();
    }

    public void updateStockItemQuantity(long stockItemID, int newQuantity) {
        Transaction transaction = session.beginTransaction();
        StockItem stockItem = (StockItem) session.get(StockItem.class, stockItemID);
        stockItem.setQuantity(newQuantity);
        session.update(stockItem);
        transaction.commit();
    }
}
