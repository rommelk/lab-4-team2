package ee.ut.math.tvt.salessystem.domain.controller.impl;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.Date;
import java.util.List;

import ee.ut.math.tvt.salessystem.domain.data.Sale;
import ee.ut.math.tvt.salessystem.domain.exception.VerificationFailedException;
import ee.ut.math.tvt.salessystem.domain.controller.SalesDomainController;
import ee.ut.math.tvt.salessystem.domain.data.SoldItem;
import ee.ut.math.tvt.salessystem.domain.data.StockItem;
import ee.ut.math.tvt.salessystem.service.HibernateDataService;
import ee.ut.math.tvt.salessystem.util.HibernateUtil;

/**
 * Implementation of the sales domain controller.
 */
public class SalesDomainControllerImpl implements SalesDomainController {

	private HibernateDataService service = new HibernateDataService();

	private static SalesDomainController singelton = new SalesDomainControllerImpl();

	public static SalesDomainController getInstance() {
		return singelton;
	}

	public void endSession() {
		System.out.println("Closing the database session.");
		HibernateUtil.closeSession();
	}

	public void submitCurrentPurchase(List<SoldItem> shoppingCart) throws VerificationFailedException {

		Date date = new Date();

		double purchasesum = 0;
		for (int i = 0; i < shoppingCart.size(); i++) {
			purchasesum += shoppingCart.get(i).getSum();
		}

		Sale sale = new Sale((long) 10, date, purchasesum, shoppingCart);

		//ADD PURCHASEITEM TO THE DATABASE
		service.addSale(sale);

		//ADD SOLDITEMS TO THE DATABASE
		for (SoldItem elem : shoppingCart) {
			elem.setSale(sale);
			service.addSoldItem(elem);
		}

		// Let's assume we have checked and found out that the buyer is underaged and
		// cannot buy chupa-chups
		//throw new VerificationFailedException("Underaged!");
		// XXX - Save purchase
	}

	public void cancelCurrentPurchase() throws VerificationFailedException {				
		// XXX - Cancel current purchase
	}
	

	public void startNewPurchase() throws VerificationFailedException {
		// XXX - Start new purchase
	}


	public List<Sale> loadHistoryState() {
		List<Sale> saleItems = service.getSaleitems();
		return saleItems;
	}

	@Override
	public void updateWarehouse(StockItem stockItem) {
		//KONTROLLI KAS OLEMAS V�I EI, VASTAVALT SIIS K�ITU
		service.addStockItem(stockItem);
	}

	public List<StockItem> loadWarehouseState() {
		// XXX mock implementation
/*		List<StockItem> dataset = new ArrayList<StockItem>();

		StockItem chips = new StockItem(1l, "Lays chips", "Potato chips", 11.0, 5);
		StockItem chupaChups = new StockItem(2l, "Chupa-chups", "Sweets", 8.0, 8);
	    StockItem frankfurters = new StockItem(3l, "Frankfurters", "Beer sauseges", 15.0, 12);
	    StockItem beer = new StockItem(4l, "Free Beer", "Student's delight", 0.0, 100);

		dataset.add(chips);
		dataset.add(chupaChups);
		dataset.add(frankfurters);
		dataset.add(beer);
		
		return dataset;*/
		return service.getStockitems();
	}

	public void updateStockItemQuantity(long stockItemID, int newQuantity) {
		service.updateStockItemQuantity(stockItemID, newQuantity);
	}

	public void addStockItem(StockItem stockItem) {
		service.addStockItem(stockItem);
	}
}
