package ee.ut.math.tvt.salessystem.domain.data;

import ee.ut.math.tvt.salessystem.domain.data.StockItem;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class StockItemTest {
    private StockItem item1;

    @Before
    public void setUp() {
        item1 = new StockItem(50L, "cider", "alcohol", 0.95, 20);
    }

    @Test
    public void testGetColumn() {
        assertEquals(item1.getColumn(0), 50L);
        assertEquals(item1.getColumn(1), "cider");
        assertEquals((Double) item1.getColumn(2), 0.95, 0.0001);
        assertEquals(item1.getColumn(3), 20);
    }

    @Test (expected = RuntimeException.class)
    public void testGetColumnThrowsError() {
        item1.getColumn(4);
        item1.getColumn(5);
    }

    @Test
    public void testClone() {
        StockItem clone = (StockItem) item1.clone();
        assertEquals(item1.getName(), clone.getName());
        assertEquals(item1.getId(), clone.getId());
        assertEquals(item1.getDescription(), clone.getDescription());
        assertEquals(item1.getPrice(), clone.getPrice(), 0.0001);
        assertEquals(item1.getQuantity(), clone.getQuantity());
    }
}
