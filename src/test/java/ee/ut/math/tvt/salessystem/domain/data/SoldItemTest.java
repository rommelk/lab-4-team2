package ee.ut.math.tvt.salessystem.domain.data;

import static org.junit.Assert.assertEquals;

import java.util.Date;

import org.junit.Before;
import org.junit.Test;

import ee.ut.math.tvt.salessystem.domain.data.Sale;
import ee.ut.math.tvt.salessystem.domain.data.SoldItem;
import ee.ut.math.tvt.salessystem.domain.data.StockItem;



public class SoldItemTest {
    private SoldItem solditem1;
    private SoldItem solditem2;
    private SoldItem solditem3;
    /**
     * Methods with @Before annotations will be invoked before each test is run.
     */
    @Before
    public void setUp() {
        StockItem stockitem = new StockItem(10L, "Nutella", "Chocolate cream", 6.35);
        StockItem stockitem2 = new StockItem(11L, "Cucumber", "Fresh", 1.35);
        solditem1 = new SoldItem(stockitem, 2);
        solditem2 = new SoldItem(stockitem, 0);
        solditem3 = new SoldItem(stockitem2,5);
    }

    @Test
    public void testGetSum() {
        assertEquals(solditem1.getSum(),12.7,0.0001);
        assertEquals(solditem3.getSum(),6.75,0.0001);
    }

    @Test
    public void testGetSumWithZeroQuantity(){
        assertEquals(solditem2.getSum(),0,0.0001);
    }
}