package ee.ut.math.tvt.salessystem.domain.data;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertEquals;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import ee.ut.math.tvt.salessystem.domain.data.Sale;
import ee.ut.math.tvt.salessystem.domain.data.SoldItem;
import ee.ut.math.tvt.salessystem.domain.data.StockItem;

public class SaleTest {
    private static SoldItem solditem1;
    private SoldItem solditem2;
    private SoldItem solditem3;
    private StockItem stockitem;
    private StockItem stockitem2;
    private Date date;
    private long id;

    @Before
    public void setUp() {
        id = 10;
        stockitem = new StockItem(id, "Nutella", "Chocolate cream", 6.35);
        stockitem2 = new StockItem(id, "Cucumber", "Fresh", 1.35);
        solditem1 = new SoldItem( stockitem, 2);
        solditem2 = new SoldItem( stockitem, 0);
        solditem3 = new SoldItem(stockitem2,5);
        date = new Date();
    }

    @Test
    public void testAddSoldItem(){
        List<SoldItem> list = new ArrayList<>();
        list.add(solditem1);
        Sale sale = new Sale(id,date,0,list);
        sale.addSoldItem(solditem2);
        assertEquals(sale.getShoppingCart().contains(solditem2), true);
    }

    @Test
    public void testGetSumWithNoItems(){
        List<SoldItem> list = new ArrayList<>();
        Sale sale = new Sale(id, date, list);
        assertEquals(sale.getTotalSum(),0,0.0001);
    }

    @Test
    public void testGetSumWithOneItem(){
        List<SoldItem> list = new ArrayList<>();
        list.add(solditem1);
        Sale sale = new Sale(id,date,list);
        assertEquals(sale.getTotalSum(),12.7,0.0001);
    }

    @Test
    public void testGetSumWithMultipleItems(){
        List<SoldItem> list = new ArrayList<>();
        list.add(solditem1);
        list.add(solditem2);
        list.add(solditem3);
        Sale sale = new Sale(id,date,list);
        assertEquals(sale.getTotalSum(),19.45,0.0001);
        sale.addSoldItem(solditem3);
        assertEquals(sale.getTotalSum(),26.2,0.0001);
    }
}