package ee.ut.math.tvt.salessystem.ui.tabs;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Objects;

import ee.ut.math.tvt.salessystem.domain.controller.SalesDomainController;
import ee.ut.math.tvt.salessystem.domain.data.StockItem;
import ee.ut.math.tvt.salessystem.ui.model.SalesSystemModel;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBoxBuilder;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class StockTab extends AnchorPane {
	private final SalesDomainController domainController;
	@FXML private Button startAddingItemToWarehouseButton;
	@FXML private Button addItemToWarehouseButton;
	@FXML private ComboBox itemBox;
	@FXML private TextField quantityField;
	@FXML private TextField nameField;
	@FXML private TextField priceField;
	@FXML private TextField descField;
	@FXML private TextField barcodeField;

	@FXML private TableView warehouseTableView;

	private SalesSystemModel model;
	private long barCodeElem;

	public StockTab(SalesSystemModel model, SalesDomainController controller) {
		super();
		FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("StockTab.fxml"));
		fxmlLoader.setRoot(this);
		fxmlLoader.setController(this);

		try {
			fxmlLoader.load();
		} catch (IOException exception) {
			throw new RuntimeException(exception);
		}
		this.domainController = controller;
		this.model = model;

		warehouseTableView.setItems(model.getWarehouseTableModel());
		disableProductField(true);

		List<StockItem> stockItems = domainController.loadWarehouseState();
		//for (StockItem item : stockItems) {
		//	System.out.println(item.toString());
		//}


		this.itemBox.focusedProperty().addListener(new ChangeListener<Boolean>() {

			@Override
			public void changed(ObservableValue<? extends Boolean> arg0, Boolean oldPropertyValue, Boolean newPropertyValue) {
				itemBox.setEditable(true);
				if (itemBox.getItems().size() == 0) {
					itemBox.getItems().add("New product");
				}
				List<String> items = new ArrayList<String>();
				for (int i = 0; i < model.getWarehouseTableModel().getRowCount(); i++) {
					items.add(model.getWarehouseTableModel().get(i).getName());
				}
				for (int i = 0; i < items.size(); i++) {
					if (itemBox.getItems().size() == 0) {
						itemBox.getItems().add(items.get(i));
					} else {
						boolean itemIsInTheList = false;
						for (int j = 0; j < itemBox.getItems().size(); j++) {
							if (items.get(i).equals(itemBox.getItems().get(j))) {
								itemIsInTheList = true;
								break;
							}
						}
						if (!itemIsInTheList) {
							itemBox.getItems().add(items.get(i));
						}
					}
				}

				itemBox.getSelectionModel().selectedItemProperty().addListener(new ChangeListener() {
					@Override
					public void changed(ObservableValue observable, Object oldValue, Object newValue) {
						barCodeElem = 0;
						for (int i = 0; i < model.getWarehouseTableModel().getRowCount(); i++) {
							if (model.getWarehouseTableModel().get(i).getName().equals(newValue)) {
								barCodeElem = model.getWarehouseTableModel().get(i).getId();
								break;
							}
						}
						if (barCodeElem != 0) {
							fillDialogFields();
						}
						else {
							quantityField.setText("");
							nameField.setText("");
							priceField.setText("");
							descField.setText("");
							barcodeField.setText("");
							barcodeField.setText(String.valueOf(model.getWarehouseTableModel().getRowCount() + 1));
						}
					}
				});
			}
		});
	}

	@FXML public void startAddingItemToWarehouse() {
		disableProductField(false);
		startAddingItemToWarehouseButton.setDisable(true);
		System.out.println(itemBox.getValue());
	}
	@FXML public void addItemToWarehouseEventHandler() {
		StockItem stockItem = getStockItemByBarcode();


		//ARE ALL FIELDS FILLED?
		if (Objects.equals(barcodeField.getText(), "") || Objects.equals(quantityField.getText(), "") || Objects.equals(nameField.getText(), "") || Objects.equals(priceField.getText(), "") || Objects.equals(descField.getText(), "")) {
			Stage dialogStage = new Stage();
			dialogStage.initModality(Modality.WINDOW_MODAL);
			dialogStage.setScene(new Scene(VBoxBuilder.create().
					children(new Text("Fill all the fields!")).
					alignment(Pos.CENTER).padding(new Insets(5)).build()));
			dialogStage.show();
			return;
		}


		//IS QUANITY A POSITIVE INTEGER?
		int quantity;
		try {
			quantity = Integer.parseInt(quantityField.getText());
		} catch (NumberFormatException ex) {
			quantity = -1;
		}
		if (quantity < 0) {
			Stage dialogStage = new Stage();
			dialogStage.initModality(Modality.WINDOW_MODAL);
			dialogStage.setScene(new Scene(VBoxBuilder.create().
					children(new Text("Check the amount.")).
					alignment(Pos.CENTER).padding(new Insets(5)).build()));
			dialogStage.show();
			return;
		}

		//IS BARCODE A POSITIVE LONG?
		long barcode;
		try {
			barcode = Long.parseLong(barcodeField.getText());
		} catch (NumberFormatException exc) {
			barcode = -1;
		}
		if (barcode <= 0) {
			Stage dialogStage = new Stage();
			dialogStage.initModality(Modality.WINDOW_MODAL);
			dialogStage.setScene(new Scene(VBoxBuilder.create().
					children(new Text("Check the barcode.")).
					alignment(Pos.CENTER).padding(new Insets(5)).build()));
			dialogStage.show();
			return;
		}

		//IS PRICE A NON-NEGATIVE FLOAT?
		double price;
		try {
			price = Double.parseDouble(priceField.getText());
		} catch (NumberFormatException ex) {
			price = -1;
		}
		if (price < 0) {
			Stage dialogStage = new Stage();
			dialogStage.initModality(Modality.WINDOW_MODAL);
			dialogStage.setScene(new Scene(VBoxBuilder.create().
					children(new Text("Check the price.")).
					alignment(Pos.CENTER).padding(new Insets(5)).build()));
			dialogStage.show();
			return;
		}


		if (stockItem == null) {
			StockItem newItem = new StockItem(barcode, nameField.getText(), descField.getText(), price, quantity);
			model.getWarehouseTableModel().add(newItem);
			domainController.addStockItem(newItem);
		}
		else {
			//CHECKING IF ALL FIELDS ARE CORRECT (matching the previous stockitem's data)
			if (barcode != stockItem.getId() || !Objects.equals(nameField.getText(), stockItem.getName()) || price != stockItem.getPrice() || !Objects.equals(descField.getText(), stockItem.getDescription())) {
				Stage dialogStage = new Stage();
				dialogStage.initModality(Modality.WINDOW_MODAL);
				dialogStage.setScene(new Scene(VBoxBuilder.create().
						children(new Text("Check the fields.")).
						alignment(Pos.CENTER).padding(new Insets(5)).build()));
				dialogStage.show();
				return;
			}
			int newQuantity = 0;
			int tableSize = model.getWarehouseTableModel().getRowCount();
			for (int i = 0; i < tableSize; i++) {
				if (model.getWarehouseTableModel().get(i).getName().equals(stockItem.getName())) {
					int oldQuantity = model.getWarehouseTableModel().get(i).getQuantity();
					model.getWarehouseTableModel().get(i).setQuantity(oldQuantity + quantity);
					newQuantity = oldQuantity + quantity;
					break;
				}
			}
			//Update the table
			List<StockItem> stockItems = new ArrayList<>();
			for (int i = 0; i < model.getWarehouseTableModel().getRowCount(); i++) {
				stockItems.add(model.getWarehouseTableModel().get(i));
			}
			model.getWarehouseTableModel().clear();
			for (int i = 0; i < stockItems.size(); i++) {
				model.getWarehouseTableModel().add(stockItems.get(i));
			}

			domainController.updateStockItemQuantity(stockItem.getId(), newQuantity);
		}

		disableProductField(true);
		startAddingItemToWarehouseButton.setDisable(false);
		resetProductField();
	}

	public void fillDialogFields() {
		StockItem stockItem = getStockItemByBarcode();
		if (stockItem != null) {
			nameField.setText(stockItem.getName());
			String priceString = String.valueOf(stockItem.getPrice());
			priceField.setText(priceString);
			quantityField.setText("");
			descField.setText(stockItem.getDescription());
			String barcodeString = String.valueOf(stockItem.getId());
			barcodeField.setText(barcodeString);
		} else {
			resetProductField();
		}
	}

	private StockItem getStockItemByBarcode() {
		try {
			return model.getWarehouseTableModel().getItemById(barCodeElem);
		} catch (NumberFormatException ex) {
			return null;
		} catch (NoSuchElementException ex) {
			return null;
		}
	}

	//Disable dialog fields.
	public void disableProductField(boolean disable) {
		this.addItemToWarehouseButton.setDisable(disable);
		this.itemBox.setDisable(disable);
		this.quantityField.setDisable(disable);
		this.nameField.setDisable(disable);
		this.priceField.setDisable(disable);
		this.descField.setDisable(disable);
		this.barcodeField.setDisable(disable);
	}

	/**
	 * Reset dialog fields.
	 */
	public void resetProductField() {
		itemBox.setValue("Add item");
		quantityField.setText("");
		nameField.setText("");
		priceField.setText("");
		descField.setText("");
		barcodeField.setText("");
	}

//
//  // warehouse stock tab - consists of a menu and a table
//  public Component draw() {
//    JPanel panel = new JPanel();
//    panel.setBorder(BorderFactory.createLineBorder(Color.BLACK));
//
//    GridBagLayout gb = new GridBagLayout();
//    GridBagConstraints gc = new GridBagConstraints();
//    panel.setLayout(gb);
//
//    gc.fill = GridBagConstraints.HORIZONTAL;
//    gc.anchor = GridBagConstraints.NORTH;
//    gc.gridwidth = GridBagConstraints.REMAINDER;
//    gc.weightx = 1.0d;
//    gc.weighty = 0d;
//
//    panel.add(drawStockMenuPane(), gc);
//
//    gc.weighty = 1.0;
//    gc.fill = GridBagConstraints.BOTH;
//    panel.add(drawStockMainPane(), gc);
//    return panel;
//  }
//
//  // warehouse menu
//  private Component drawStockMenuPane() {
//    JPanel panel = new JPanel();
//
//    GridBagConstraints gc = new GridBagConstraints();
//    GridBagLayout gb = new GridBagLayout();
//
//    panel.setLayout(gb);
//
//    gc.anchor = GridBagConstraints.NORTHWEST;
//    gc.weightx = 0;
//
//    addItem = new JButton("Add");
//    gc.gridwidth = GridBagConstraints.RELATIVE;
//    gc.weightx = 1.0;
//    panel.add(addItem, gc);
//
//    panel.setBorder(BorderFactory.createLineBorder(Color.BLACK));
//    return panel;
//  }
//
//
//  // table of the wareshouse stock
//  private Component drawStockMainPane() {
//    JPanel panel = new JPanel();
//
//    JTable table = new JTable(model.getWarehouseTableModel());
//
//    JTableHeader header = table.getTableHeader();
//    header.setReorderingAllowed(false);
//
//    JScrollPane scrollPane = new JScrollPane(table);
//
//    GridBagConstraints gc = new GridBagConstraints();
//    GridBagLayout gb = new GridBagLayout();
//    gc.fill = GridBagConstraints.BOTH;
//    gc.weightx = 1.0;
//    gc.weighty = 1.0;
//
//    panel.setLayout(gb);
//    panel.add(scrollPane, gc);
//
//    panel.setBorder(BorderFactory.createTitledBorder("Warehouse status"));
//    return panel;
//  }

}
