package ee.ut.math.tvt.salessystem.ui.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ListIterator;

import ee.ut.math.tvt.salessystem.domain.data.Sale;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

//import org.apache.log4j.Logger;

import ee.ut.math.tvt.salessystem.domain.data.SoldItem;
import ee.ut.math.tvt.salessystem.domain.data.StockItem;
import ee.ut.math.tvt.salessystem.ui.SalesSystemUI;

public class HistoryTableModel extends SalesSystemTableModel<Sale>{
    private static final long serialVersionUID = 1L;

    public HistoryTableModel() {
        super(defineTableColumns());
    }

    public static List<TableColumn<Sale, ?>> defineTableColumns(){
        List<TableColumn<Sale, ?>> columnList = new ArrayList<TableColumn<Sale, ?>>();

        TableColumn<Sale, Date> timeCol = new TableColumn<Sale, Date>("Time of the order");
        timeCol.setCellValueFactory(new PropertyValueFactory<Sale,Date>("saleTime"));

        TableColumn<Sale, Double> totalpriceCol = new TableColumn<Sale, Double>("Total order price");
        totalpriceCol.setCellValueFactory(new PropertyValueFactory<Sale,Double>("totalSum"));

        columnList.add(timeCol);
        columnList.add(totalpriceCol);

        return columnList;
    }


    public Object getColumnValue(Sale item, int columnIndex) {
        switch (columnIndex) {
            case 0:
                return item.getSaleTime();
            case 1:
                return item.getTotalSum();

        }
        throw new IllegalArgumentException("Column index out of range");
    }
    public void addItem(final Sale item) {
        add(item);
    }

    @Override
    public String toString() {
        final StringBuffer buffer = new StringBuffer();

        for (int i = 0; i < headers.size(); i++)
            buffer.append(headers.get(i).getText() + "\t");
        buffer.append("\n");
        ListIterator<Sale> itr = listIterator();
        while(itr.hasNext()){
            final Sale item = itr.next();
            buffer.append(item.getSaleTime() + "\t");
            buffer.append(item.getTotalSum() + "\t");

            buffer.append("\n");
        }

        return buffer.toString();
    }
}
