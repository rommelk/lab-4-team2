package ee.ut.math.tvt.salessystem.ui.tabs;


import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;



public class TeamTab extends AnchorPane {

    @FXML private Label teamNameLabel;
    @FXML private Label teamLeaderLabel;
    @FXML private Label teamLeaderEmailLabel;
    @FXML private Label teamMembersLabel;
    @FXML private ImageView teamLogoView = new ImageView();
    @FXML private Label versionNumberLabel;

    public TeamTab() throws IOException {

        super();

        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("TeamTab.fxml"));
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);

        try {
            fxmlLoader.load();
            //Reading data and giving values.
            ReadValuesFromFile();
        } catch (IOException exception) {
            throw new RuntimeException(exception);
        }
    }


    private void ReadValuesFromFile() throws IOException {
        teamNameLabel.setText(getPropValues("teamName", "application.properties"));
        teamLeaderLabel.setText(getPropValues("teamLeader", "application.properties"));
        teamLeaderEmailLabel.setText(getPropValues("teamLeaderEmail", "application.properties"));
        teamMembersLabel.setText(getPropValues("teamMembers", "application.properties"));

        String imageSource = getPropValues("teamLogo", "application.properties");
        Image logoPicture = new Image(imageSource);
        teamLogoView.setImage(logoPicture);

        versionNumberLabel.setText(getPropValues("version", "version.properties"));
        System.out.println("version: " + versionNumberLabel.getText());
    }


    public String getPropValues(String data, String fileName) throws IOException {
        InputStream inputStream = null;
        String result = "";
        try {
            Properties prop = new Properties();
            inputStream = getClass().getClassLoader().getResourceAsStream(fileName);
            if (inputStream != null) {
                prop.load(inputStream);
            }
            else {
                throw new FileNotFoundException("Property file" + fileName + "not found.");
            }
            result = prop.getProperty(data);
        }
        catch (IOException ioException) {
            ioException.printStackTrace();
        }
        finally {
            assert inputStream != null;
            inputStream.close();
        }
        return result;
    }
}
