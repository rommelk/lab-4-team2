package ee.ut.math.tvt.salessystem.ui;

//import com.jgoodies.looks.windows.WindowsLookAndFeel;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import ee.ut.math.tvt.salessystem.ui.tabs.HistoryTab;
import ee.ut.math.tvt.salessystem.ui.tabs.TeamTab;
import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Side;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import ee.ut.math.tvt.salessystem.domain.controller.SalesDomainController;
import ee.ut.math.tvt.salessystem.domain.controller.impl.SalesDomainControllerImpl;
import ee.ut.math.tvt.salessystem.ui.model.SalesSystemModel;
//import ee.ut.math.tvt.salessystem.ui.tabs.HistoryTab;
import ee.ut.math.tvt.salessystem.ui.tabs.PurchaseTab;
import ee.ut.math.tvt.salessystem.ui.tabs.StockTab;

import javafx.stage.WindowEvent;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

import com.sun.javafx.css.StyleManager;

/**
 * Graphical user interface of the sales system.
 */
public class SalesSystemUI extends Application {

    private static final long serialVersionUID = 1L;

    private static final Logger log = LogManager.getLogger(SalesSystemUI.class);

    //Domain controller
    private final SalesDomainController domainController = new SalesDomainControllerImpl();;

    //Warehouse model
    private SalesSystemModel model = new SalesSystemModel(domainController);



    @Override
    public void start(Stage primaryStage) throws IOException {


//	  Application.setUserAgentStylesheet(null);
//	  StyleManager.getInstance().addUserAgentStylesheet("DefaultTheme.css");
//	  System.out.println(StyleManager.getInstance());

        primaryStage.setTitle("Sales system");
        Group root = new Group();
        Scene scene = new Scene(root, 600, 500, Color.WHITE);
        scene.getStylesheets().add(getClass().getResource("DefaultTheme.css").toExternalForm());

        System.out.println(System.getProperty("javafx.runtime.version"));

        TabPane tabPane = new TabPane();

        BorderPane borderPane = new BorderPane();
        HistoryTab history = new HistoryTab(model, domainController);
        PurchaseTab purchase = new PurchaseTab(domainController, model);
        StockTab stock = new StockTab(model, domainController);
        TeamTab team = new TeamTab();
        //PurchaseTab purchase = (PurchaseTab) FXMLLoader.load(getClass().getClassLoader().getResource("ee.ut.math.tvt.salessystem.ui.tabs.PurchaseTab.fxml"));
        //log.info(getClass().getResource("tabs/PurchaseTab.fxml"));
        //FXMLLoader loader = new FXMLLoader(getClass().getResource("tabs/PurchaseTab.fxml"));
        //loader.setController(domainController);
        //Tab purchase = loader.load();

        Tab purchaseTab = new Tab();
        purchaseTab.setText("Point-of-sale");
        purchaseTab.setClosable(false);
        purchaseTab.setContent(purchase);

        Tab stockTab = new Tab();
        stockTab.setText("Warehouse");
        stockTab.setClosable(false);
        stockTab.setContent(stock);

        Tab historyTab = new Tab();
        historyTab.setText("History");
        historyTab.setClosable(false);
        historyTab.setContent(history);

        Tab teamTab = new Tab();
        teamTab.setText("Team");
        teamTab.setClosable(false);

        teamTab.setContent(team);


        tabPane.getTabs().addAll(purchaseTab, stockTab, historyTab, teamTab);
        //tabPane.getStyleClass().add(TabPane.STYLE_CLASS_FLOATING);

        //TODO: probably not needed
        //tabPane.setTabMaxWidth((600-58)/3);
        //tabPane.setTabMinWidth((600-58)/3);

        //tabPane.setSide(Side.LEFT);

        borderPane.prefHeightProperty().bind(scene.heightProperty());
        borderPane.prefWidthProperty().bind(scene.widthProperty());

        borderPane.setCenter(tabPane);
        root.getChildren().add(borderPane);
        primaryStage.setScene(scene);
        primaryStage.show();

        log.info("Salesystem GUI started");


        primaryStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
            @Override
            public void handle(WindowEvent event) {
                domainController.endSession();
                System.out.println("Closing the window.");
                System.exit(0);
            }
        });

    }
}


