package ee.ut.math.tvt.salessystem.ui.tabs;

import java.awt.Component;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

import ee.ut.math.tvt.salessystem.domain.controller.SalesDomainController;
import ee.ut.math.tvt.salessystem.domain.data.Sale;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Tab;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TitledPane;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

import javax.swing.JPanel;

import ee.ut.math.tvt.salessystem.domain.data.SoldItem;
import ee.ut.math.tvt.salessystem.ui.model.PurchaseInfoTableModel;
import ee.ut.math.tvt.salessystem.ui.model.SalesSystemModel;
import ee.ut.math.tvt.salessystem.ui.model.SalesSystemTableModel;

/**
 * Encapsulates everything that has to do with the purchase tab (the tab
 * labelled "History" in the menu).
 */
public class HistoryTab extends AnchorPane {
    @FXML private TableView<Sale> historyOrdersTableView = new TableView<>();
    @FXML private TableView<SoldItem> newTableModel = new TableView<>();
    private final SalesDomainController domainController;


    private SalesSystemModel model;

    public HistoryTab(SalesSystemModel model, SalesDomainController domainController) {
        super();
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("HistoryTab.fxml"));
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);

        try {
            fxmlLoader.load();
        } catch (IOException exception) {
            throw new RuntimeException(exception);
        }
        this.model = model;
        this.domainController = domainController;
        historyOrdersTableView.setItems(model.getHistoryTableModel());
        newTableModel.setItems(model.getShoppingCartTableModel());

        List<Sale> sales = domainController.loadHistoryState();
        for (Sale sale : sales) {
            model.getHistoryTableModel().addItem(sale);
        }
    }


    public Component draw() {
        JPanel panel = new JPanel();
        // TODO - Sales history table
        return panel;
    }
    @FXML public void displayShoppingCart(){

        model.getShoppingCartTableModel().clear();

        for(int i = 0; i < historyOrdersTableView.getSelectionModel().getSelectedItem().getShoppingCart().size(); i++){
            SoldItem newItem = historyOrdersTableView.getSelectionModel().getSelectedItem().getShoppingCart().get(i);
            model.getShoppingCartTableModel().addItem(newItem);
        }
    }










}