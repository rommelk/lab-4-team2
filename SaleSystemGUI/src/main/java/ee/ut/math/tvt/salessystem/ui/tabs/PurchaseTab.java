package ee.ut.math.tvt.salessystem.ui.tabs;

import ee.ut.math.tvt.salessystem.domain.data.Sale;
import ee.ut.math.tvt.salessystem.domain.data.SoldItem;
import ee.ut.math.tvt.salessystem.domain.data.StockItem;
import ee.ut.math.tvt.salessystem.domain.exception.VerificationFailedException;
import ee.ut.math.tvt.salessystem.domain.controller.SalesDomainController;
import ee.ut.math.tvt.salessystem.ui.model.SalesSystemModel;

import java.io.IOException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.*;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;

import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBoxBuilder;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

/**
 * Encapsulates everything that has to do with the purchase tab (the tab
 * labelled "Point-of-sale" in the menu).
 */
public class PurchaseTab extends AnchorPane {

    private static final Logger log = LogManager.getLogger(PurchaseTab.class);

    private final SalesDomainController domainController;

    @FXML private Button newPurchase;
    @FXML private Button submitPurchase;
    @FXML private Button cancelPurchase;

    @FXML private ComboBox nameBox;
    @FXML private TextField quantityField;
    @FXML private TextField barCodeField;
    @FXML private TextField priceField;

    @FXML private Button addItemButton;

    @FXML private TableView purchaseTableView;

    private long barCodeElem;

    private SalesSystemModel model;

    private Map<Long, Integer> quantityMap;

//  public PurchaseTab(){
//	  super();
//	  domainController = SalesDomainControllerImpl.getInstance();
//	  model = new SalesSystemModel(domainController);
//  }



    public PurchaseTab(SalesDomainController controller,
                       SalesSystemModel model)
    {
        super();
        this.domainController = controller;
        this.model = model;

        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("PurchaseTab.fxml"));
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);

        try {
            fxmlLoader.load();
        } catch (IOException exception) {
            throw new RuntimeException(exception);
        }

        //Disable cancel and submit button
        cancelPurchase.setDisable(true);
        submitPurchase.setDisable(true);
        purchaseTableView.setItems(model.getCurrentPurchaseTableModel());
        disableProductField(true);


        this.nameBox.focusedProperty().addListener(new ChangeListener<Boolean>() {

            @Override
            public void changed(ObservableValue<? extends Boolean> arg0, Boolean oldPropertyValue, Boolean newPropertyValue) {
                nameBox.setEditable(true);

                List<String> items = new ArrayList<String>();
                for (int i = 0; i < model.getWarehouseTableModel().getRowCount(); i++) {
                    items.add(model.getWarehouseTableModel().get(i).getName());
                }
                for (int i = 0; i < items.size(); i++) {
                    if (nameBox.getItems().size() == 0) {
                        nameBox.getItems().add(items.get(i));
                    } else {
                        boolean itemIsInTheList = false;
                        for (int j = 0; j < nameBox.getItems().size(); j++) {
                            if (items.get(i).equals(nameBox.getItems().get(j))) {
                                itemIsInTheList = true;
                                break;
                            }
                        }
                        if (!itemIsInTheList) {
                            nameBox.getItems().add(items.get(i));
                        }
                    }
                }

                nameBox.getSelectionModel().selectedItemProperty().addListener(new ChangeListener() {
                    @Override
                    public void changed(ObservableValue observable, Object oldValue, Object newValue) {
                        for (int i = 0; i < model.getWarehouseTableModel().getRowCount(); i++) {
                            if (model.getWarehouseTableModel().get(i).getName().equals(newValue)) {
                                barCodeElem = model.getWarehouseTableModel().get(i).getId();
                                break;
                            }
                        }

                        fillDialogFields();
                    }
                });
            }
        });
    }



    /**
     * The purchase tab. Consists of the purchase menu, current purchase dialog and
     * shopping cart table.

    */


//  /** Event handler for the <code>new purchase</code> event. */
    @FXML protected void newPurchaseButtonClicked() {
        quantityMap = new HashMap<>();
        for (int i = 0; i < model.getWarehouseTableModel().getRowCount(); i++) {
            quantityMap.put(model.getWarehouseTableModel().get(i).getId(), model.getWarehouseTableModel().get(i).getQuantity());
        }
        log.info("New sale process started");
        try {
            domainController.startNewPurchase();
            startNewSale();
        } catch (VerificationFailedException e1) {
            log.error(e1.getMessage());
        }
    }


    /**  Event handler for the <code>cancel purchase</code> event. */
    @FXML protected void cancelPurchaseButtonClicked() {
        log.info("Sale cancelled");
        for (int i = 0; i < model.getWarehouseTableModel().getRowCount(); i++) {
            model.getWarehouseTableModel().get(i).setQuantity(quantityMap.get(model.getWarehouseTableModel().get(i).getId()));
            System.out.println(model.getWarehouseTableModel().get(i).getName() + "/" + model.getWarehouseTableModel().get(i).getQuantity());
        }

        try {
            domainController.cancelCurrentPurchase();
            endSale();
            model.getCurrentPurchaseTableModel().clear();
        } catch (VerificationFailedException e1) {
            log.error(e1.getMessage());
        }
    }


    /** Event handler for the <code>submit purchase</code> event. */
    @FXML protected void submitPurchaseButtonClicked() {
        DecimalFormat df = new DecimalFormat("#.##");
        TitledPane titledPane = new TitledPane();
        titledPane.setText("Current order");
        GridPane gp = new GridPane();
        gp.setStyle("-fx-background-color: #A9F5A9;");

        Label totalSum = new Label("Total sum: ");
        totalSum.setFont(Font.font("Verdana", FontWeight.BOLD, 12));
        gp.add(totalSum,0,0);
        //Here I calculate total sum, I take every row sum value and add it to totalsum
        double sum = 0;
        for (int i = 0; i <model.getCurrentPurchaseTableModel().getRowCount() ; i++) {
            sum += model.getCurrentPurchaseTableModel().get(i).getSum();
        }
        //will show the total sum of the order
        Label sumLabel = new Label(df.format(sum));
        sumLabel.setFont(Font.font("Verdana",15));
        gp.add(sumLabel, 2, 0);
        //will have a field to enter the payment amount
        Label paymentAmountLabel = new Label("Payment amount: ");
        paymentAmountLabel.setFont(Font.font("Verdana", FontWeight.BOLD, 12));
        TextField paymentAmount = new TextField();
        gp.add(paymentAmountLabel, 0, 4);
        gp.add(paymentAmount, 2, 4);

        Button calculate = new Button("Calculate");
        calculate.setFont(Font.font("Verdana", FontWeight.BOLD, 12));;
        gp.add(calculate, 3, 4);

        //Will show the change amount
        Label returnMoneyLabel = new Label("Change amount: ");
        gp.add(returnMoneyLabel, 0, 6);
        returnMoneyLabel.setFont(Font.font("Verdana", FontWeight.BOLD, 12));
        Label returnMoney = new Label();
        gp.add(returnMoney, 2, 6);

        System.out.println(paymentAmount.getText());

        //Buttons to accept and cancel the payment
        Button accept = new Button("Accept");
        accept.setPrefWidth(100);
        accept.setPrefHeight(10);
        accept.setFont(Font.font("Verdana", FontWeight.BOLD, 12));
        accept.setDisable(true);
        Button cancel = new Button("Cancel");
        cancel.setPrefWidth(100);
        cancel.setFont(Font.font("Verdana", FontWeight.BOLD, 12));
        gp.add(accept, 0, 8);
        gp.add(cancel, 2, 8);


        titledPane.setContent(gp);
        Scene scene = new Scene(titledPane);
        Stage popupStage = new Stage();
        popupStage.setScene(scene);
        popupStage.setHeight(180);
        popupStage.setWidth(400);
        popupStage.setResizable(false);

        popupStage.show();


        //If calculate is cliked then it will show return money.
        calculate.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if (paymentAmount.getText() != null && !paymentAmount.getText().isEmpty()) {
                    double sum1 = Double.parseDouble(sumLabel.getText());
                    double amount;
                    try {
                        amount = Double.parseDouble(paymentAmount.getText());
                    } catch (NumberFormatException e) {
                        amount = -1.0;
                    }

                    if (amount < 0) {
                        System.out.println("Check the money.");
                    } else {
                        double changeAmount = amount - sum1;
                        if (changeAmount >= 0){
                            returnMoney.setText(df.format(changeAmount));
                            accept.setDisable(false);
                        }
                    }
                }
            }
        });
        //Cancel button close the popup window.
        cancel.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                popupStage.close();
            }
        });

        //Accept button actionevent
        accept.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {

                double changeAmount = Double.parseDouble(returnMoney.getText());

                if (returnMoney.getText()!=null && Double.parseDouble(returnMoney.getText())>= 0){
                    System.out.println("Everything is okei, you can successfully accept the payment!");

                    //SOMEWHERE HERE WE MUST CHANGE THE WAREHOUSE DATA VALUE
                    //Update the warehouse
                    List<StockItem> stockItems = new ArrayList<>();
                    for (int i = 0; i < model.getWarehouseTableModel().getRowCount(); i++) {
                        stockItems.add(model.getWarehouseTableModel().get(i));
                    }
                    model.getWarehouseTableModel().clear();
                    for (int i = 0; i < stockItems.size(); i++) {
                        model.getWarehouseTableModel().add(stockItems.get(i));
                    }


                    //----------------------------------------------------------------------
                    //In here we should add the payment info to history
                    List<SoldItem> shoppingCart = new ArrayList<SoldItem>();
                    for(int i=0;i<model.getCurrentPurchaseTableModel().getRowCount();i++){
                        SoldItem soldItem = new SoldItem(model.getCurrentPurchaseTableModel().get(i).getStockItem(),model.getCurrentPurchaseTableModel().get(i).getQuantity());
                        shoppingCart.add(soldItem);
                    }
                    try {
                        domainController.submitCurrentPurchase(shoppingCart);
                    } catch (VerificationFailedException e) {
                        log.error(e.getMessage());
                    }

                    List<Sale> sales = domainController.loadHistoryState();
                    model.getHistoryTableModel().clear();
                    for (Sale sale : sales) {
                        model.getHistoryTableModel().addItem(sale);
                    }

                    //----------------------------------------------------------------------

                    popupStage.close();
                    log.info("Sale cancelled");
                    try {
                        domainController.cancelCurrentPurchase();
                        endSale();
                        model.getCurrentPurchaseTableModel().clear();
                    } catch (VerificationFailedException e1) {
                        log.error(e1.getMessage());
                    }

                }
                else {
                    Label message = new Label("Check return money amount!!");
                    gp.add(message,0,7);
                }
            }
        });


    }



  /* === Helper methods that bring the whole purchase-tab to a certain state
   *     when called.
   */

    // switch UI to the state that allows to proceed with the purchase
    private void startNewSale() {
        resetProductField();
        disableProductField(false);
        cancelPurchase.setDisable(false);
        submitPurchase.setDisable(false);
        newPurchase.setDisable(true);
    }

    // switch UI to the state that allows to initiate new purchase
    private void endSale() {
        resetProductField();
        cancelPurchase.setDisable(true);
        submitPurchase.setDisable(true);
        newPurchase.setDisable(false);
        disableProductField(true);
    }

    //Fill dialog with data from the "database".
    public void fillDialogFields() {
        StockItem stockItem = getStockItemByBarcode();
        if (stockItem != null) {
            String barCodeString = String.valueOf(stockItem.getId());
            barCodeField.setText(barCodeString);
            String priceString = String.valueOf(stockItem.getPrice());
            priceField.setText(priceString);
        } else {
            resetProductField();
        }
    }

    // Search the warehouse for a StockItem with the bar code entered
    // to the barCode textfield.
    private StockItem getStockItemByBarcode() {
        try {

            return model.getWarehouseTableModel().getItemById(barCodeElem);

        } catch (NumberFormatException ex) {
            return null;
        } catch (NoSuchElementException ex) {
            return null;
        }

    }

    /* Add new item to the cart.
    */
    @FXML public void addItemEventHandler() {
        // add chosen item to the shopping cart.
        StockItem stockItem = getStockItemByBarcode();
        System.out.println("algne seis: " + stockItem.getQuantity());
        //QUANITY
        if (stockItem != null) {
            int quantity;
            try {
                quantity = Integer.parseInt(quantityField.getText());
            } catch (NumberFormatException ex) {
                quantity = -1;
            }
            int uusSeis = stockItem.getQuantity() - quantity;
            System.out.println(uusSeis);
            if (quantity <= 0) {
                Stage dialogStage = new Stage();
                dialogStage.initModality(Modality.WINDOW_MODAL);
                dialogStage.setScene(new Scene(VBoxBuilder.create().
                        children(new Text("Check the quantity.")).
                        alignment(Pos.CENTER).padding(new Insets(5)).build()));
                dialogStage.show();
                return;
            }
            else if (stockItem.getQuantity() - quantity < 0) {
                Stage dialogStage = new Stage();
                dialogStage.initModality(Modality.WINDOW_MODAL);
                dialogStage.setScene(new Scene(VBoxBuilder.create().
                        children(new Text("There aren't enough products in the warehouse.")).
                        //children(new Text("Laos pole piisaval koguses antud toodet."), new Button("OK")).
                        alignment(Pos.CENTER).padding(new Insets(5)).build()));
                dialogStage.show();
                return;
            }


            //PRICE
            double price;
            try {
                price = Double.parseDouble(priceField.getText());
            } catch (NumberFormatException ex) {
                price = -1;
            }
            if (price != stockItem.getPrice()) {
                Stage dialogStage = new Stage();
                dialogStage.initModality(Modality.WINDOW_MODAL);
                dialogStage.setScene(new Scene(VBoxBuilder.create().
                        children(new Text("Price is not matching the one in the warehouse.")).
                        alignment(Pos.CENTER).padding(new Insets(5)).build()));
                dialogStage.show();
                return;
            }


            //Checking if the barcode is correct
            long barcode;
            try {
                barcode = Long.parseLong(barCodeField.getText());
            } catch (NumberFormatException exc) {
                barcode = -1;
            }
            if (barcode != stockItem.getId()) {
                Stage dialogStage = new Stage();
                dialogStage.initModality(Modality.WINDOW_MODAL);
                dialogStage.setScene(new Scene(VBoxBuilder.create().
                        children(new Text("Barcode is not matching with the barcode in warehouse.")).
                        alignment(Pos.CENTER).padding(new Insets(5)).build()));
                dialogStage.show();
                return;
            }

            if (model.getCurrentPurchaseTableModel().getRowCount() == 0) {
                SoldItem newItem = new SoldItem(stockItem, quantity);
                model.getCurrentPurchaseTableModel().add(newItem);

                //update the warehouse
                for (int i = 0; i < model.getWarehouseTableModel().getRowCount(); i++) {
                    if (model.getWarehouseTableModel().get(i).getId() == stockItem.getId()) {
                        int oldWarehouseQuantity = model.getWarehouseTableModel().get(i).getQuantity();
                        model.getWarehouseTableModel().get(i).setQuantity(oldWarehouseQuantity - quantity);
                    }
                }
            } else {
                for (int i = 0; i < model.getCurrentPurchaseTableModel().getRowCount(); i++) {
                    if (model.getCurrentPurchaseTableModel().get(i).getStockItem().getId() == stockItem.getId()) {
                        int oldQuantity = model.getCurrentPurchaseTableModel().get(i).getQuantity();
                        model.getCurrentPurchaseTableModel().remove(i);
                        SoldItem newItem = new SoldItem(stockItem, oldQuantity + quantity);
                        model.getCurrentPurchaseTableModel().add(newItem);

                        //update the warehouse
                        for (int j = 0; j < model.getWarehouseTableModel().getRowCount(); j++) {
                            if (model.getWarehouseTableModel().get(j).getId() == stockItem.getId()) {
                                int oldWarehouseQuantity = model.getWarehouseTableModel().get(j).getQuantity();
                                model.getWarehouseTableModel().get(j).setQuantity(oldWarehouseQuantity - quantity);
                            }
                        }
                        return;
                    }
                }
                SoldItem newItem = new SoldItem(stockItem, quantity);
                model.getCurrentPurchaseTableModel().add(newItem);

                //update the warehouse
                for (int i = 0; i < model.getWarehouseTableModel().getRowCount(); i++) {
                    if (model.getWarehouseTableModel().get(i).getId() == stockItem.getId()) {
                        int oldWarehouseQuantity = model.getWarehouseTableModel().get(i).getQuantity();
                        model.getWarehouseTableModel().get(i).setQuantity(oldWarehouseQuantity - quantity);
                    }
                }
            }


/*            Boolean notExict = true;

            if(!model.getCurrentPurchaseTableModel().isEmpty()){
                int modelSize = model.getCurrentPurchaseTableModel().getRowCount();
                for (int i = 0; i < modelSize ; i++) {
                    if(model.getCurrentPurchaseTableModel().get(i).getName().equals(stockItem.getName())){
                        System.out.println("algne seis2: " + model.getWarehouseTableModel().get(i).getQuantity());
                        StockItem item = model.getCurrentPurchaseTableModel().get(i).getStockItem();
                        int oldQuantity = model.getCurrentPurchaseTableModel().get(i).getQuantity();
                        model.getCurrentPurchaseTableModel().remove(i);
                        model.getCurrentPurchaseTableModel().addItem(new SoldItem(item,oldQuantity+quantity));

                        notExict = false;
                        int warehouseOldQuantity = model.getWarehouseTableModel().get(i).getQuantity();
                        model.getWarehouseTableModel().get(i).setQuantity(warehouseOldQuantity-quantity);
                        System.out.println("uus seis: " + model.getWarehouseTableModel().get(i).getQuantity());
                        return;
                    }
                }
            }
            if(notExict=true){
                model.getCurrentPurchaseTableModel()
                        .addItem(new SoldItem(stockItem, quantity));

            }

            for (int i = 0; i < model.getWarehouseTableModel().getRowCount(); i++) {
                if (Objects.equals(model.getWarehouseTableModel().get(i).getId(), stockItem.getId())) {
                    int oldQuantity = stockItem.getQuantity();
                    model.getWarehouseTableModel().get(i).setQuantity(oldQuantity - quantity);
                    System.out.println("uus seis: " + model.getWarehouseTableModel().get(i).getQuantity());
                    break;
                }
            }*/
        }
    }

    /**
     * Sets whether or not the product component is enabled.
     */
    public void disableProductField(boolean disable) {
        this.addItemButton.setDisable(disable);
        this.nameBox.setDisable(disable);
        this.quantityField.setDisable(disable);
        this.barCodeField.setDisable(disable);
        this.priceField.setDisable(disable);
    }

    /**
     * Reset dialog fields.
     */
    public void resetProductField() {

        nameBox.setValue(null);
        quantityField.setText("1");
        barCodeField.setText("");
        priceField.setText("");
    }
}
