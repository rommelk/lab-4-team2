package ee.ut.math.tvt.salessystem.ui.model;

import ee.ut.math.tvt.salessystem.domain.data.StockItem;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.assertEquals;
import java.util.NoSuchElementException;


public class StockTableModelTest {

    private StockItem item;
    private StockTableModel stockTableModel;

    @Before
    public void setUp() {
        item = new StockItem(50L, "candy Kaseke", "sweets", 0.95, 20);
        stockTableModel = new StockTableModel();
        stockTableModel.addItem(item);
    }

    @Test
    public void testGetColumnValue() {
        assertEquals(stockTableModel.getColumnValue(item, 0), 50L);
        assertEquals(stockTableModel.getColumnValue(item, 1), "candy Kaseke");
        assertEquals((Double) stockTableModel.getColumnValue(item, 2), 0.95, 0.0001);
        assertEquals(stockTableModel.getColumnValue(item, 3), 20);
    }

    @Test (expected = IllegalArgumentException.class)
    public void testGetColumnValueThrowsError() {
        stockTableModel.getColumnValue(item, 4);
        stockTableModel.getColumnValue(item, 5);
    }

    @Test
    public void testGetItemByIdWhenItemExists(){
        assertEquals(stockTableModel.getItemById(50L), item);
    }

    @Test (expected = NoSuchElementException.class)
    public void testGetItemByIdWhenThrowsException(){
        StockItem stockItem = stockTableModel.getItemById(200L);
    }

    @Test
    public void testGetRowCount() {
        assertEquals(stockTableModel.getRowCount(), 1);
    }

    @Test
    public void testGetValueAt() {
        assertEquals(stockTableModel.getValueAt(0, 0), 50L);
        assertEquals(stockTableModel.getValueAt(0, 1), "candy Kaseke");
        assertEquals((Double) stockTableModel.getValueAt(0, 2), 0.95, 0.0001);
        assertEquals(stockTableModel.getValueAt(0, 3), 20);
    }

    @Test
    public void testAddItemWhenItemExists() {
        stockTableModel.addItem(item);
        assertEquals(stockTableModel.getItemById(50L).getQuantity(), 40); //quantity increased
    }

    @Test
    public void testAddItemWhenItemDoesNotExist() {
        StockItem newItem = new StockItem(60L, "apple", "fruit", 0.20, 40);
        stockTableModel.addItem(newItem);
        assertEquals(stockTableModel.getItemById(60L), newItem); //item exists now
    }
}