package ee.ut.math.tvt.salessystem.ui.model;

import ee.ut.math.tvt.salessystem.domain.data.Sale;
import ee.ut.math.tvt.salessystem.domain.data.SoldItem;
import ee.ut.math.tvt.salessystem.domain.data.StockItem;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class PurchaseInfoTableModelTest {

    private PurchaseInfoTableModel purchaseInfoTableModel;
    private SoldItem soldItem;
    private StockItem stockItem;

    @Before
    public void setUp() {
        purchaseInfoTableModel = new PurchaseInfoTableModel();
        stockItem = new StockItem(50L, "apple", "fruit", 0.20, 20);
        soldItem = new SoldItem(100L, new Sale(), stockItem, stockItem.getName(), 20, stockItem.getPrice());
    }

    @Test
    public void testAddItem() {
        SoldItem soldItem2 = new SoldItem(20L, new Sale(), stockItem, stockItem.getName(), 50, stockItem.getPrice());
        purchaseInfoTableModel.addItem(soldItem2);
        assertEquals(purchaseInfoTableModel.getItemById(20L), soldItem2);
    }

    @Test
    public void testGetColumnValue() {
        purchaseInfoTableModel.addItem(soldItem);
        assertEquals(purchaseInfoTableModel.getColumnValue(soldItem, 0), 100L);
        assertEquals(purchaseInfoTableModel.getColumnValue(soldItem, 1), "apple");
        assertEquals((Double) purchaseInfoTableModel.getColumnValue(soldItem, 2), 0.20, 0.0001);
        assertEquals(purchaseInfoTableModel.getColumnValue(soldItem, 3), 20);
    }

    @Test (expected = IllegalArgumentException.class)
    public void testGetColumnValueThrowsError() {
        purchaseInfoTableModel.addItem(soldItem);
        purchaseInfoTableModel.getColumnValue(soldItem, 4);
        purchaseInfoTableModel.getColumnValue(soldItem, 5);
    }
}
