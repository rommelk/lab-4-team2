package ee.ut.math.tvt.salessystem.ui.model;

import ee.ut.math.tvt.salessystem.domain.data.Sale;
import ee.ut.math.tvt.salessystem.domain.data.SoldItem;
import ee.ut.math.tvt.salessystem.domain.data.StockItem;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class HistoryTableModelTest {
    private HistoryTableModel historyTableModel;
    private Date date;
    private Sale sale;

    @Before
    public void setUp() {
        historyTableModel = new HistoryTableModel();
        date = new Date();
        sale = new Sale(100L, date, 0, new ArrayList<>());
    }

    @Test
    public void testAddItem() {
        Sale newSale = new Sale(50L, new Date(), 0, new ArrayList<>());
        historyTableModel.addItem(newSale);
        assertEquals(historyTableModel.getItemById(50L), newSale);
    }

    @Test
    public void testGetColumnValue() {
        historyTableModel.addItem(sale);
        assertEquals(historyTableModel.getColumnValue(sale, 0), date);
        assertEquals((Double) historyTableModel.getColumnValue(sale, 1), 0, 0.0001);
    }

    @Test (expected = IllegalArgumentException.class)
    public void testGetColumnValueThrowsError() {
        historyTableModel.addItem(sale);
        historyTableModel.getColumnValue(sale, 2);
        historyTableModel.getColumnValue(sale, 3);
    }
}
